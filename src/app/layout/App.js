import React from "react";
import { Container } from "semantic-ui-react";
import EventDashboard from "../../features/event/EeventDashboard/EventDashboard";
import NavBar from "../../features/nav/NavBar/NavBar";

function App() {
  return (
    <div>
      <NavBar></NavBar>
      <Container className="main">
        <EventDashboard></EventDashboard>
      </Container>
    </div>
  );
}

export default App;
